package io.catalyte.training.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.catalyte.training.entities.Pet;
import io.catalyte.training.exceptions.BadDataResponse;
import io.catalyte.training.exceptions.ResourceNotFound;
import io.catalyte.training.exceptions.ServiceUnavailable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;

import static io.catalyte.training.constants.StringConstants.CONTEXT_VACCINATIONS;

import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static io.catalyte.training.constants.StringConstants.CONTEXT_PETS;

@SpringBootTest
@AutoConfigureMockMvc
class PetControllerTest {

    ResultMatcher okStatus = MockMvcResultMatchers.status().isOk();
    ResultMatcher createdStatus = MockMvcResultMatchers.status().isCreated();
    ResultMatcher deletedStatus = MockMvcResultMatchers.status().isNoContent();
    ResultMatcher notFoundStatus = MockMvcResultMatchers.status().isNotFound();
    ResultMatcher badRequestStatus = MockMvcResultMatchers.status().isBadRequest();
    ResultMatcher dataErrorStatus = MockMvcResultMatchers.status().isServiceUnavailable();
    ResultMatcher serverErrorStatus = MockMvcResultMatchers.status().isInternalServerError();
    ResultMatcher expectedType = MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON);

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    public void setUp(){
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();
    }

    @Test
    public void postAllPets() throws Exception {
        List<String> json = new ArrayList<>();
        json.add(
                "{\"id\":5,\"name\":\"Harry\",\"breed\":\"Dog\",\"age\":12}");
        json.add(
                "{\"id\":6,\"name\":\"Potter\",\"breed\":\"Dog\",\"age\":2}");

        this.mockMvc
                .perform(post(CONTEXT_PETS + "/all")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(json)))
                .andExpect(createdStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$", isA(ArrayList.class)))
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void getPetsReturns() throws Exception {
        mockMvc
                .perform(get(CONTEXT_PETS))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$",hasSize(greaterThan(1))));
    }

    @Test
    public void putPet() throws Exception {
        String json = "{\"id\":1,\"name\":\"Bevis\",\"breed\":\"Dog\",\"age\":6}";
        this.mockMvc
                .perform(put(CONTEXT_PETS + "/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.name", is("Bevis")));
    }

    @Test
    public void getPetThatDoesExistByID() throws Exception{
        mockMvc
                .perform(get(CONTEXT_PETS + "/2"))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.name", is("Alexander Bunnington")));
    }

    @Test
    public void getPetBadRequest() throws Exception{
        mockMvc
                .perform(get(CONTEXT_PETS + "/imaginaryFriend"))
                .andExpect(badRequestStatus);
    }

    @Test
    public void getPetThatDoesNotExist() throws Exception{
        mockMvc
                .perform(get(CONTEXT_PETS + "/8"))
                .andExpect(notFoundStatus);
    }

    @Test
    public void postNewPet() throws Exception {
        //Pet pet = new Pet();
        String json = "{\"id\":4,\"name\":\"Clifford\",\"breed\":\"Dog\",\"age\":10}";

       // String petAsJson =mapper.writeValueAsString(pet);
        this.mockMvc
                .perform(post(CONTEXT_PETS)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(createdStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.name", is("Clifford")));
    }

    @Test
    public void deletePet() throws Exception {
        mockMvc
                .perform(delete(CONTEXT_PETS + "/4"))
                .andExpect(deletedStatus);
    }
}